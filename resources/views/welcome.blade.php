@extends('layouts.layout-index')

@section('content')
<div class="container mt-4">
    <div class="row">
        <div class="col-12 mt-5">
            <h1 class="mb-4">Тестовое задание</h1>
            <p>Выберите занятое время сотрудника</p>
            <div id="workingWindows" class="timeSeances">
                @foreach($all as $item)
                    <div class="btn btn-sm btn-primary" onclick="addTime(event)">{{$item}}</div>
                @endforeach
            </div>
            <div id="workingTime" class="timeSeances d-none">
                <input id="seanceLength" type="number" placeholder="Время выполнения услуги в сек." class="form-control">
                <button onclick="seanceLengthAdd()" type="button" class="mt-3 btn btn-primary">Добавить</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-6 d-none" id="selectDateSeance">
            <p>Выберите дату сеанса</p>
            <input id="dateUser" onchange="dateSelect()" type="date" placeholder="Дата" class="form-control">
            <button onclick="userAction()" type="button" class="mt-3 btn btn-primary">Показать свободное время</button>
        </div>
        <div class="col-6">
            <code id="codeSeances"></code>
        </div>
        <div class="col-12 ">
            <div id="danger" class="text-danger d-none"></div>
            <div id="timeSelect" class="timeSeances mt-5"></div>
        </div>
    </div>
</div>
@endsection
