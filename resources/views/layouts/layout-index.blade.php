<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>WorkSeances</title>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    @vite(['resources/css/app.css'])

</head>
<body>
@yield('content')

<script>
    var timeWaitingSeancesLength = {};

    var GlobalSeances = {
        seances: [],
        date: ''
    };

    function clearMessageError(){
        document.getElementById('danger').classList.toggle('d-none');
    }

    function addMessageError(message){
        document.getElementById('danger').innerHTML = message;
        document.getElementById('danger').classList.toggle('d-none');
    }

    function changeDataCodeSeances(){
        document.getElementById('codeSeances').innerHTML = JSON.stringify(GlobalSeances);
    }

    function viewWindowsOrTimeWorkingUser(){
        document.getElementById('workingWindows').classList.toggle('d-none');
        document.getElementById('workingTime').classList.toggle('d-none');
    }

    function viewSelectDateSeance(){
        document.getElementById('selectDateSeance').classList.toggle('d-none');
    }

    function seanceLengthAdd(){
        const seanceLength = document.getElementById('seanceLength').value
        if(seanceLength == 0){
            addMessageError('Поле время выполнения обязательно для заполнения!');
            return false;
        }
        viewWindowsOrTimeWorkingUser()
        this.GlobalSeances.seances.unshift(
            {
                time: timeWaitingSeancesLength,
                seance_length: seanceLength
            }
        )
        document.getElementById('seanceLength').value = '';

        if (document.getElementById('selectDateSeance').classList.contains('d-none')) {
            viewSelectDateSeance()
        }
        changeDataCodeSeances()
    }

    function addTime(event) {
        const date = event.target.innerHTML;
        if (event.target.classList.contains('btn-primary')) {
            event.target.classList.add('btn-secondary');
            event.target.classList.remove('btn-primary');
            viewWindowsOrTimeWorkingUser();
            this.timeWaitingSeancesLength = date;
        } else {
            event.target.classList.remove('btn-secondary');
            event.target.classList.add('btn-primary');
            this.GlobalSeances.seances = this.GlobalSeances.seances.filter(el => el.time !== date);

            if(!this.GlobalSeances.seances.length){
                viewSelectDateSeance()
            }

            changeDataCodeSeances()
        }
    }

    function dateSelect(){
        const dateUser = document.getElementById('dateUser').value;
        this.GlobalSeances.date = Date.parse(dateUser);
        changeDataCodeSeances()
    }

    const userAction = async () => {
        if (!this.GlobalSeances.date) {
            addMessageError('Выберите дату')
            return false;
        }
        clearMessageError()
        const settings = {
            method: 'POST',
            body: JSON.stringify(this.GlobalSeances),
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        };
        document.getElementById("timeSelect").innerHTML = '';
        fetch("/api/free-seances/", settings).then(function (response) {
            if (response.ok) {
                response.json().then(function (json) {
                    clearMessageError()
                    Object.keys(json).forEach(function (key) {
                        let div = document.createElement('div'); // is a node
                        div.innerHTML = json[key];
                        div.classList = 'btn btn-sm btn-success';
                        document.getElementById("timeSelect").appendChild(div);
                    });

                });
            } else {
                response.json().then(function (json) {
                    addMessageError(json.message)
                });
            }
        });
    }

</script>
</body>
</html>
