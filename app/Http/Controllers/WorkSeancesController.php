<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApiNewSeancesRequest;
use App\Services\BookingService;
use Illuminate\Support\Arr;

class WorkSeancesController extends Controller
{
    public function index(BookingService $bookingService)
    {
        return view('welcome', [
            'all' => $bookingService->getWorkingWindows()
        ]);
    }

    public function freeSeances(ApiNewSeancesRequest $request, BookingService $bookingService)
    {
        return response()->json($bookingService->getFreeWorkingWindows(Arr::get($request, 'seances')), 200);
    }
}
