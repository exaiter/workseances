<?php

namespace App\Services;

use Carbon\Carbon;
use DateTime;

class BookingService
{
    protected string $startWorkingDay = '10:00';
    protected string $endWorkingDay = '20:00';
    protected int $intervalUser = 45;
    protected int $intervalWorking = 5;

    public function getWorkingWindows(): array
    {
        $startOfDay = (new Carbon($this->startWorkingDay));
        $endOfDay = (new Carbon($this->endWorkingDay))->modify("-$this->intervalUser minutes");
        $currentTime = $startOfDay;
        $all = [];
        while ($currentTime <= $endOfDay) {
            $all[] = $currentTime->format('H:i');
            $currentTime = $currentTime->modify("+$this->intervalWorking minutes");
        }

        return $all;
    }

    public function getFreeWorkingWindows(array $seances): array
    {
        $busy = [];
        $addTimeCount = ceil($this->intervalUser / $this->intervalWorking);
        foreach ($seances as $item) {
            $startBusy = (new Carbon($item['time']));
            $startBusy->modify("-$this->intervalUser minutes");
            $count = ceil($item['seance_length'] / 60 / $this->intervalWorking)+$addTimeCount;
            for ($i = 0; $i < $count-1; $i++) {
                $startBusy->modify("+$this->intervalWorking minutes");
                $busy[] = $startBusy->format('H:i');
            }
        }

        return array_diff($this->getWorkingWindows(), $busy);
    }
}
